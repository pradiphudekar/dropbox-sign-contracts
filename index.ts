import * as DropboxSign from "@dropbox/sign";
import { SignatureRequestResponse } from "@dropbox/sign";
import * as fs from 'fs';
import path from "path";
import { pathToFileURL } from "url";

const signatureRequestApi = new DropboxSign.SignatureRequestApi();

if (process.argv.length === 2) {
    console.error('API_KEY missing');
    process.exit(1);
}

// Configure HTTP basic authorization: api_key
signatureRequestApi.username = process.argv[2];

getAllSignatureRequests().then(signatureRequests => {
    for (let i = 0; i < signatureRequests.length; i++) {
        setTimeout(() => downloadFiles(signatureRequests[i]), 3000 * i);
    };
});

async function getAllSignatureRequests() {
    let signatureRequests: SignatureRequestResponse[] = [];
    let totalPages = 0;
    let currentPage = 1;

    do {
        const resp: DropboxSign.SignatureRequestListResponse = await getSignatureRequests(currentPage);
        totalPages = resp.listInfo?.numPages || 0;
        signatureRequests = signatureRequests.concat(resp.signatureRequests || []);
        printProgress(`Fetched ${currentPage} of ${totalPages}`)
        await sleep(3000)
        currentPage++;
    } while (currentPage <= totalPages)

    console.log(signatureRequests.map(s => `"${s.signatureRequestId}"`).join(","));
    console.log("Found signature total " + signatureRequests.length + " signature requests.");
    return signatureRequests;
}

async function getSignatureRequests(page: number): Promise<DropboxSign.SignatureRequestListResponse> {
    const result = signatureRequestApi.signatureRequestList(undefined, page);
    return result.then(response => {
        return response.body;
    }).catch(error => {
        console.log("Exception when calling Dropbox Sign API:");
        throw (error);
    });
}

async function getSignatureRequestById(id: string): Promise<DropboxSign.SignatureRequestResponse | undefined> {
    const result = signatureRequestApi.signatureRequestGet(id);
    return result.then(response => {
        return response.body.signatureRequest;
    }).catch(error => {
        console.log("Exception when calling Dropbox Sign API:");
        throw (error);
    });
}


async function downloadFiles(signatureRequest: SignatureRequestResponse) {
    const fileType = "pdf";
    const outputDir = path.join(__dirname, "contracts");
    if (!fs.existsSync(outputDir)) {
        fs.mkdirSync(outputDir);
    }
    const filename = `${signatureRequest.title}_${signatureRequest.signatureRequestId}.${fileType}`;
    const result = signatureRequestApi.signatureRequestFiles(signatureRequest.signatureRequestId || '', fileType);
    return result.then(response => {
        fs.createWriteStream(pathToFileURL(path.join(outputDir, `metadata_${signatureRequest.signatureRequestId}.json`))).write(JSON.stringify(signatureRequest));
        fs.createWriteStream(pathToFileURL(path.join(outputDir, filename.replace(/\//g, '\u2215')))).write(response.body);
        console.log("Success: ", filename)
    }).catch(error => {
        console.log("Exception when calling Dropbox Sign API for :", path.join(outputDir, filename));
        throw (error);
    });
}

async function downloadFileForId(signatureRequestId: string) {
    return getSignatureRequestById(signatureRequestId)
        .then(signatureRequest => {
            if (signatureRequest) {
                return downloadFiles(signatureRequest)
            } else {
                throw "Could not find signature request"
            }
        });
}

async function sleep(milliseconds: number) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}

function printProgress(progress: string) {
    process.stdout.clearLine(0);
    process.stdout.cursorTo(0);
    process.stdout.write(progress);
}